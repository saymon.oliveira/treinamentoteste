package TreinamentoTeste.BrowserStack;

import TreinamentoTeste.BrowserStack.Screens.HomeScreen;
import TreinamentoTeste.BrowserStack.Screens.InputControlsScreen;
import TreinamentoTeste.BrowserStack.Screens.MenuScreen;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class AppSampleTests extends TestBase {


    HomeScreen homeScreen;
    MenuScreen menuScreen;
    InputControlsScreen inputControlsScreen;


    @Test
    public void verificaTextField() throws IOException, InterruptedException {

        homeScreen = new HomeScreen();
        menuScreen = new MenuScreen();
        inputControlsScreen = new InputControlsScreen();

        //Arrange
        String inputText = "Preenchimento do campo teste";

        //Clicar no menu
        homeScreen.clicarMenu();

        //Clicar no menu “Input Controls”
        menuScreen.clicarMenuInputControl();

        //Preencher campo TextField
        inputControlsScreen.fillTextField(inputText);

        //Verificar se o texto digitado está no campo
        Assert.assertEquals(inputControlsScreen.returnTextField(),inputText);
    }

    @Test
    public void verificaCheckBox() throws IOException, InterruptedException {

        homeScreen = new HomeScreen();
        menuScreen = new MenuScreen();
        inputControlsScreen = new InputControlsScreen();

        //Arrange
        String expectedText = "Checked";

        //Clicar no menu
        homeScreen.clicarMenu();

        //Clicar no menu “Input Controls”
        menuScreen.clicarMenuInputControl();

        //Clicar no menu “Acessar CheckBox”
        inputControlsScreen.clickCheckBoxTitle();

        //Clicar checkbox
        inputControlsScreen.clickCheckBox();

        //Verificar se o texto digitado está no campo
        Assert.assertEquals(inputControlsScreen.returnCheckBoxText(),expectedText);
    }

    @Test
    public void verificaRadioButton() throws IOException, InterruptedException {

        WebDriverWait wait = new WebDriverWait(TestBase.driver, 60);

        //Arrange
        String expectedText = "Web";

        homeScreen = new HomeScreen();
        menuScreen = new MenuScreen();
        inputControlsScreen = new InputControlsScreen();

        //Clicar no menu
        homeScreen.clicarMenu();

        //Clicar no menu “Input Controls”
        menuScreen.clicarMenuInputControl();

        //Clicar no menu “Acessar CheckBox”
        inputControlsScreen.clickCheckBoxTitle();

        //Clicar checkbox
        inputControlsScreen.clickCheckBox();

        //Clicar no menu “Acessar Radio Buttons”
        inputControlsScreen.clickRadioButtonMenu();

        //Clicar radio Web
        inputControlsScreen.clickRadioButtonWeb();

        //Verificar se o texto digitado está no campo
        Assert.assertEquals(inputControlsScreen.returnRadioButtonChooseText(),expectedText);
        
    }

}
