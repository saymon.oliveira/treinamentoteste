package TreinamentoTeste.BrowserStack.Screens;

import TreinamentoTeste.BrowserStack.Relatorio;
import TreinamentoTeste.BrowserStack.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MenuScreen {

    private AppiumDriver driver = null;
    private WebDriverWait wait = null;

    public MenuScreen(){
        driver = TestBase.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
        wait = new WebDriverWait (driver, 60);
    }

    @AndroidFindBy(xpath = "(//android.widget.TextView[@content-desc=\"Row Category Name\"])[4]")
    private MobileElement inputControlMenu;


    public void clicarMenuInputControl(){
        wait.until(ExpectedConditions.elementToBeClickable(inputControlMenu));
        inputControlMenu.click();
        Relatorio.gravaInformacaoRelatorio("Click Menu Input Control");
    }



}
