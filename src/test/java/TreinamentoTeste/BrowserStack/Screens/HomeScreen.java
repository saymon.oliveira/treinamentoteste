package TreinamentoTeste.BrowserStack.Screens;

import TreinamentoTeste.BrowserStack.Relatorio;
import TreinamentoTeste.BrowserStack.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomeScreen {

    private AppiumDriver driver = null;
    private WebDriverWait wait = null;

    public HomeScreen(){
        driver = TestBase.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
        wait = new WebDriverWait (driver, 60);
    }


    @AndroidFindBy(xpath = "//android.widget.ImageButton[@content-desc=\"ReferenceApp\"]")
    private MobileElement menuButton;


    public void clicarMenu(){
        menuButton.click();
        Relatorio.gravaInformacaoRelatorio("Click Menu");
    }





}
