package TreinamentoTeste.BrowserStack.Screens;
import TreinamentoTeste.BrowserStack.Relatorio;
import TreinamentoTeste.BrowserStack.TestBase;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InputControlsScreen {

    private AppiumDriver driver = null;
    private WebDriverWait wait = null;

    public InputControlsScreen(){
        driver = TestBase.getDriver();
        PageFactory.initElements(new AppiumFieldDecorator(driver),this);
        wait = new WebDriverWait (driver, 60);
    }


    @AndroidFindBy(xpath = "//android.widget.EditText[@content-desc=\"Text Input Control\"]")
    private MobileElement inputTextField;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Checkbox']")
    private MobileElement checkBoxTitle;

    @AndroidFindBy(xpath = "//android.widget.CheckBox[@content-desc='Checkbox Control']")
    private MobileElement checkBox;

    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"Checkbox Display\"]")
    private MobileElement checkBoxText;

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Radio Buttons']")
    private MobileElement radioButtonTitle;

    @AndroidFindBy(xpath = "//android.widget.RadioButton[@content-desc=\"Radio Button 2\"]")
    private MobileElement radioButtonWeb;

    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc=\"Radio Button Display\"]")
    private MobileElement radioButtonChooseText;

    public void fillTextField(String value){
        inputTextField.sendKeys(value);
        Relatorio.gravaInformacaoRelatorio("Preenche Text Field ||" + value);
    }

    public String returnTextField(){
        return inputTextField.getText();
    }

    public void clickCheckBoxTitle(){
        wait.until(ExpectedConditions.elementToBeClickable(checkBoxTitle));
        checkBoxTitle.click();
        Relatorio.gravaInformacaoRelatorio("Click CheckBox Tittle");
    }

    public void clickCheckBox(){
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.CheckBox[@content-desc='Checkbox Control']")));
        wait.until(ExpectedConditions.elementToBeClickable(checkBox));
        checkBox.click();
        Relatorio.gravaInformacaoRelatorio("Click CheckBox");
    }

    public String returnCheckBoxText(){
        String text = checkBoxText.getText();
        Relatorio.gravaInformacaoRelatorio("Return CheckBox Text ||" + text);
        return text;
    }

    public void clickRadioButtonMenu() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Radio Buttons']")));
        wait.until(ExpectedConditions.elementToBeClickable(radioButtonTitle));
        radioButtonTitle.click();
        Relatorio.gravaInformacaoRelatorio("Click RadioButton");
    }

    public void clickRadioButtonWeb() {
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.RadioButton[@content-desc=\"Radio Button 2\"]")));
        wait.until(ExpectedConditions.elementToBeClickable(radioButtonWeb));
        radioButtonWeb.click();
        Relatorio.gravaInformacaoRelatorio("Click RadioButton Web");
    }

    public String returnRadioButtonChooseText(){
        String text = radioButtonChooseText.getText();
        Relatorio.gravaInformacaoRelatorio("Return RadioButton Text ||" + text);
        return text;
    }

}
