package TreinamentoTeste.BrowserStack;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;

public class TestBase {

    public static AppiumDriver<MobileElement> driver;

    public static AppiumDriver<MobileElement> getDriver() {
        return driver;
    }

    @BeforeSuite
    public void oneTimeBefore() {
        Relatorio.iniciaRelatorio();
    }

    @BeforeMethod
    public void beforeTest(Method method)throws IOException {
        inicializaDriver(true);
        Relatorio.criaTesteRelatorio(method.getName());
    }

    public static void inicializaDriver(boolean deviceFarm)throws IOException {
        DesiredCapabilities caps = new DesiredCapabilities();

        if(deviceFarm){
            String userName = "teste2223";
            String accessKey = "expoFphXidiTxvYTKdGZ";
            caps.setCapability("device", "Google Pixel 3");
            caps.setCapability("os_version", "9.0");
            caps.setCapability("project", "My First Project");
            caps.setCapability("build", "My First Build");
            caps.setCapability("name", "Bstack-[Java] Sample Test");
            caps.setCapability("app", "bs://f04df04ddab90727f6ec30a9768c055dcbe42f91");

            driver = new AndroidDriver<MobileElement>(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"), caps);
        }
        else {
            caps.setCapability("platformName", "Android");
            caps.setCapability("platformVersion", "9");
            caps.setCapability("deviceName", "Pixel_2_API_28");
            caps.setCapability("app", "C:\\Repositorio de Projetos\\Treinamentos\\apks\\appSample.apk");

            driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
        }
    }

    @AfterMethod
    public void afterTest(ITestResult result){
        Relatorio.adicionaResultadoTeste(result);
        driver.quit();
    }

    @AfterSuite
    public void afterSuite(){
        Relatorio.geraRelatorio();
    }


}
